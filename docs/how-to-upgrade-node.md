## Updating Q-Client & Docker Images

In case of severe updates of the Q-Client, you will be required to update the validator files and configs. To do so, within directory `/testnet-validator` (for validator), `/testnet-rootnode` (for rootnode) or `/testnet-fullnode` (for fullnode), use the following commands:

1. Change the docker image directly in your **.env** file:
```
...
QCLIENT_IMAGE=qblockchain/q-client:1.2.2
...
```

2. Pull (and overwrite) the latest docker image 
```text
$ docker-compose pull
```

3. Restart with new configs & images
```text
$ docker-compose up -d
```

Now your validator node should restart and synchronise with the testnet again.

