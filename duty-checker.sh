#!/usr/bin/bash

# This script allow you to check what validator is duty now.
# It retrieve network and BridgeValidator contract address as parameters.
# For now we have validator list mock instead of retrieveing real validator list

# TODO: retrieve latest block number and return address with `block.number % validatorCount` index
# devhint: eth_blockNumber

NETWORK=${1:-https://rpc.qtestnet.org:8545/}
BV=${2:-0xa59693Fe62217b3e888b79844A9E88B46f6BFf97}
# TODO: get and parse validator list via BaseBridgeValidators.validatorList function (data:0x5890ef79)
validator_list=("64d4edefe8ba86d3588b213b0a053e7b910cad68" "6a39b688d591ea00c9ea69658438794204b5cc62" "4a14d788d86d021670ebcece1196631d66595984")

is_validator_duty() {
	address=$1
	start_data="0x8e4ec60a000000000000000000000000"
	data="${start_data}${address}"
	result=$(curl -s --location --request POST $NETWORK \
		--header 'Content-Type: application/json' \
		--data-raw '{"jsonrpc":"2.0", "method": "eth_call", "params": 
		[
    			{
        			"to": '\"$BV\"',
        			"data": '\"$data\"'
    			},
   			 "latest"
		],
		"id": 1}' \
	 |  jq '.result')
	echo ${result: -2: -1}
}

are_validators_duty() {
	for i in ${!validator_list[@]}; do
		address="${validator_list[$i]}"
		if [ $(is_validator_duty $address) == "1" ]; then
			echo $i "0x${address}"
		fi
	done
}

test_duty() {
	END=$1
	while true; do
		now=$(date)
		echo "================${now}================"
		are_validators_duty
	done
}

test_duty $TEST_COUNT
